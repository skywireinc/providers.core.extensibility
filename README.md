# SkyWire.Providers.Core.Extensibility

Extensibility package to integrate with SkyWire systems using providers. This package includes Provider interfaces, exceptions, and requests/responses.