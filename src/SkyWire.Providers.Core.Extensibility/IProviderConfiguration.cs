﻿namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents configuration options that are available for a user to provide.
    /// </summary>
    public interface IProviderConfiguration
    {
    }
}
