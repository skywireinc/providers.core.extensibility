﻿namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents the request data to be handled by a provider.
    /// </summary>
    public interface IProviderRequest
    {
        /// <summary>
        /// Transforms the request into the given type. 
        /// </summary>
        /// <typeparam name="TRequest">The type of request object.</typeparam>
        /// <returns>A transformed object for the given type.</returns>
        TRequest GetRequest<TRequest>()
            where TRequest : class, new();
    }
}
