﻿using System;

namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents a set of helper methods to extend functionality with providers and their communication.
    /// </summary>
    public static class ProviderHelpers
    {
        /// <summary>
        /// Appends a custom data object to the exception to be used to add additional details to a providers error.
        /// </summary>
        /// <typeparam name="TException">The type of exception.</typeparam>
        /// <param name="exception">The exception to add the data to.</param>
        /// <param name="data">The data that will be serialized into the exceptions data.</param>
        /// <returns>The given exception with added data.</returns>
        public static TException WithData<TException>(this TException exception, object data)
            where TException : Exception
        {
            var json = data?.ToJson() ?? throw new ArgumentNullException(nameof(data));

            exception.Data.Add(Constants.EXCEPTION_KEY, json);

            return exception;
        }
    }
}
