﻿namespace SkyWire.Providers.Core.Extensibility
{

    /// <summary>
    /// Represents a response from a provider that allows to construct the response object into a specified type. 
    /// </summary>
    /// <typeparam name="TData">The type of response object to transform.</typeparam>
    public class ProviderResponse<TData> : IProviderResponse
        where TData : class, new()
    {
        private readonly TData _responseData;

        /// <summary>
        /// Takes in a response object to be transformed into a specified type.
        /// </summary>
        /// <param name="response">Response object created by a provider that should be associated with a request.</param>
        public ProviderResponse(TData response)
        {
            _responseData = response;
        }

        /// <summary>
        /// Transforms the response object into the given type.
        /// </summary>
        /// <typeparam name="TResponse">The Response object type expected from the provider.</typeparam>
        /// <returns>A transformed object for the given type.</returns>
        public TResponse GetResponse<TResponse>()
            where TResponse : class, new() => _responseData?.ToJson().FromJson<TResponse>();
    }
}
