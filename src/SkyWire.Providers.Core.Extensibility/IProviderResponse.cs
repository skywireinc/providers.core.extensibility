﻿namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents the response object and status of a provider. 
    /// </summary>
    public interface IProviderResponse
    {
        /// <summary>
        /// Gets the response object that was generated in the request.
        /// </summary>
        /// <typeparam name="TResponse">The Request object type expected for the provider.</typeparam>
        /// <returns>A deserialized object for the given type.</returns>
        TResponse GetResponse<TResponse>()
            where TResponse : class, new();
    }
}
