﻿using Newtonsoft.Json;
using System;

namespace SkyWire.Providers.Core.Extensibility
{
    internal static class GenericExtensions
    {
        public static string ToJson<T>(this T source) => JsonConvert.SerializeObject(source);
        public static T FromJson<T>(this string json) => JsonConvert.DeserializeObject<T>(json);
    }
}
