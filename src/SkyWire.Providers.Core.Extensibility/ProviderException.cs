﻿using System;

namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents an exception thrown by a provider with a error message and status code.
    /// </summary>
    public class ProviderException : Exception
    {
        private readonly int _statusCode;

        /// <summary>
        /// Exception with error message and statusCode
        /// </summary>
        /// <param name="errorMessage">Indicates the cause of the exception.</param>
        /// <param name="statusCode">Indicates the status of the exception.</param>
        public ProviderException(string errorMessage, int statusCode)
            : base(errorMessage)
        {
            _statusCode = statusCode;
        }

        /// <summary>
        /// Gets the status code for the exception.
        /// </summary>
        public int StatusCode => _statusCode; 
    }
}
