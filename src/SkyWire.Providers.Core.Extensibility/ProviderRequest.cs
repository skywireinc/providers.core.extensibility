﻿namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents a request to a provider that allows to construct the request object into a specified type.
    /// </summary>
    /// <typeparam name="TData">The type of request object to transform.</typeparam>
    public class ProviderRequest<TData> : IProviderRequest
        where TData : class, new()
    {
        private readonly TData _requestData;

        /// <summary>
        /// Takes in a request object to be transformed into a specified type.
        /// </summary>
        /// <param name="request">The request object to transform.</param>
        public ProviderRequest(TData request)
        {
            _requestData = request;
        }

        /// <summary>
        /// Transforms the request into the given type.
        /// </summary>
        /// <typeparam name="TRequest">The type of request object.</typeparam>
        /// <returns>A tranformed object for the given type.</returns>
        public TRequest GetRequest<TRequest>()
            where TRequest : class, new() => _requestData?.ToJson().FromJson<TRequest>();
    }
}
