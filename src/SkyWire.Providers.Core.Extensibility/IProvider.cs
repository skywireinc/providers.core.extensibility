﻿using System.Threading.Tasks;

namespace SkyWire.Providers.Core.Extensibility
{
    /// <summary>
    /// Represents a hook into a specific provider type to process a request and provide a successful or failed detailed response.
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// Handles the request event from the given provider type.
        /// </summary>
        /// <returns>Provider Response which provides a method to get the response object from the provider.</returns>
        Task<IProviderResponse> HandleAsync(IProviderRequest providerRequest);
    }
}
