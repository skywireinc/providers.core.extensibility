# 0.2.0

## Features

* Added support for MEF (NETSTANDARD uses `Microsoft.Composition 1.0.31`)
* Added support for DataAnnotations (NETSTANDARD uses `System.ComponentModel.Annotations 4.4.1`)

# 0.1.0

## Features

* Interfaces and DTO's available to implement a provider